import './app.css'
import Vue from 'vue'
import App from './views/App.vue'
import elementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// test dev light theme
// import '../theme/src/light/index.scss';
// test dev dark theme
// import '../theme/src/light/index.scss';
// test build light theme
// import '../theme/lib/light/index.css';
// test build dark theme
import '../theme/src/light/index.scss';
Vue.use(elementUI);
// 全局注册组件
import firstComponent from '../dist/index.js';
Vue.use(firstComponent);

new Vue({ // eslint-disable-line no-new
  el: '#app',
  render: h => h(App)
})
