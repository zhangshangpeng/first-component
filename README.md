## 简介
first component 是一个供前端开发人员学习和开发前端组件的例子，开发者可以将开发完成的组件贡献到科达的组件私库中，
为了确保自己提供的组件可用，务必要确保example中的示例可以正常运行

## 前期准备

1 Git 中 SSH key 生成步骤

* 安装git（git version 2.17.0.windows.1）
* 鼠标右键git bash here
* git config --global user.name "xxx"
* git config --global user.email "xxx@kedacom.com"
* ssh-keygen -t rsa -C "xxx@kedacom.com"(利用默认配置，直接回车)
* 找到C:\Users\xxx\.ssh 目录，里面有两个文件：id_rsa和id_rsa.pub
* gitlab -> settings -> SSH key(http://gitlab.ctsp.kedacom.com/profile/keys)下配置ssh key(copy id_rsa.pub里面的内容)
* 检查C:\Users\xxx\.ssh\known_hosts和hosts里面对于gitlab的地址解析是否正确
* 如果没有生成known_hosts，可以在git bash中执行： Git clone #自己的git仓库# --recursive，可能会弹出一个确认框，输入yes点击确认即可

2 source tree的SSH配置

* 打开Source Tree，进入 "工具" -> "选项" -> SSH客户端配置
* SSH客户端选择 OpenSSH
* SSH秘钥选择C:\Users\xxx\.ssh\id_rsa

## Build Setup

``` bash
## 安装相关依赖
npm install

## 启动测试环境
npm run dev

## 将组件的scss编译成css，将背景图片转换成base64（注意背景图标的大小应限制在20M），并copy到dist目录下。工程中引用可以通过im
npm run build

## 结构

| 目录名称 | 作用|备注|
| ---- | ---- | -----|
|config|webpack配置文件||
|build|webpack脚本文件|可运行相应脚本进行打包、启动开发服务器等操作|
|theme|存放后样式资源|light和dark分别代表两种皮肤|
|theme/lib|num run build编译和压缩后的css|
|examples|组件的测试环境|examples只是用于测试组件运行效果的，它的代码并不会构成组件的一部分，最终对外提供的只是src/index.js，所以可以在里面放心测试，在examples中尽可能的展示组件的所有功能和使用方法|
|src|组件的代码实现|组件开发完成后，记得在src/index.js中注册|
|package.json -> name|组件的名称|命名规范是@kd-components/xxx|
|package.json -> version|组件的版本后|每次升级记得修改|
|package.json -> main|组件的入口|dist/index.js|
|package.json -> dependencies|组件运行所需要的依赖|依赖该当前组件的项目npm install时会，除了安装当前组件，还会安装当前组件里dependencies里声明的组件|
|package.json -> devDependencies|组件开发所需要的依赖|只是当前开发和打包所依赖的资源|

## 注意
* 每次发布到私库，需要在package.json中修改version，然后打一个在gitlab中打一个tag
* 如果需要支持rem,在gulpfile.js中放开px2rem
* 如果需要支持换肤,务必测试所有的theme/src和theme/lib
* scss中的背景图片控制在20kb以内
* 组件支持全局注册和在vue中直接import的两种方式

` //main.js
  import firstComponent from '../src/index.js';
  Vue.use(firstComponent);`

  `//App.vue
   import {KdLabel,KdAvatar,KdButton} from '../../src/index';
   export default {
           components: {
                 KdLabel,
                 KdAvatar,
                 KdButton
           },
           ...
   }`

   1 example 是否可以运行
   2 是否需要支持换肤，如果需要，结构是否能够支持换肤
   3 组件的名字是否合法
   4 组件的版本是否合法
   5 组件依赖是否合法
   6 src的层次结构是否合法
   7 eslint是否合法
   8 scss背景图片大小是否合法
   9 readme是否详细说明了使用细节