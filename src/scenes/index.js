import KdSceneAntics from './src/mSceneAntics';
import KdSceneBeau from './src/mSceneBeau';
import KdSceneAmb from './src/mSceneAmb';

/* istanbul ignore next */
KdSceneAmb.install = function (Vue) {
  Vue.component(KdSceneAmb.name, KdSceneAmb);
};

KdSceneBeau.install = function (Vue) {
    Vue.component(KdSceneBeau.name, KdSceneBeau);
};

KdSceneAntics.install = function (Vue) {
    Vue.component(KdSceneAntics.name, KdSceneAntics);
};
export {KdSceneAmb, KdSceneAntics, KdSceneBeau};
