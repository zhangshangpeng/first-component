import KdButton from './src/button';

/* istanbul ignore next */
KdButton.install = function (Vue) {
  Vue.component(KdButton.name, KdButton);
};

export default KdButton;
