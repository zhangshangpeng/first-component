import KdAvatar from './src/avatar';

/* istanbul ignore next */
KdAvatar.install = function (Vue) {
  Vue.component(KdAvatar.name, KdAvatar);
};

export default KdAvatar;
