import KdLabel from './src/label';

/* istanbul ignore next */
KdLabel.install = function (Vue) {
  Vue.component(KdLabel.name, KdLabel);
};

export default KdLabel;
