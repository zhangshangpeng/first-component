import KdLabel from './label/index.js';
import KdButton from './button/index.js';
import KdAvatar from './avatar/index.js';
import {KdSceneAmb, KdSceneBeau, KdSceneAntics} from './scenes/index.js';
const components = [
    KdLabel,
    KdButton,
    KdAvatar,
    KdSceneAmb,
    KdSceneBeau,
    KdSceneAntics
];
const install = function (Vue, opts = {}) {
    components.forEach(component => {
        Vue.component(component.name, component);
    });
};

/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}
export {
    KdLabel,
    KdButton,
    KdAvatar,
    KdSceneAmb,
    KdSceneBeau,
    KdSceneAntics
};

export default {
    version: '1.0.2',
    install,
    KdLabel,
    KdButton,
    KdAvatar,
    KdSceneAmb,
    KdSceneBeau,
    KdSceneAntics
};
